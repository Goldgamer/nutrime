﻿using AppKit;
using Foundation;
using NutriMe;
using Sentry;
using Xamarin.Forms;
using Xamarin.Forms.Platform.MacOS;
// also add a using for the Xamarin.Forms project, if the namespace is different to this file
[Register("AppDelegate")]
public class AppDelegate : FormsApplicationDelegate
{
    NSWindow window;
    public AppDelegate()
    {
        var style = NSWindowStyle.Closable | NSWindowStyle.Resizable | NSWindowStyle.Titled;

        var rect = new CoreGraphics.CGRect(200, 1000, 1024, 768);
        window = new NSWindow(rect, style, NSBackingStore.Buffered, false);
        window.Title = "NutriMe Client"; // choose your own Title here
        window.TitleVisibility = NSWindowTitleVisibility.Hidden;
    }

    public override NSWindow MainWindow
    {
        get { return window; }
    }

    public override void DidFinishLaunching(NSNotification notification)
    {
        
        SentryXamarin.Init(Options =>
        {
            Options.Dsn = "https://e7028736217d4ea29f01a997fad3efec@o476173.ingest.sentry.io/5796783";
            Options.AddXamarinFormsIntegration();
        });
        
        Forms.Init();
        LoadApplication(new App());
        base.DidFinishLaunching(notification);
    }
    public override void WillTerminate(NSNotification notification)
    {
        // Insert code here to tear down your application
    }
}


